﻿using Leaf.xNet;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Hermes.Consts;
using static Hermes.LoggingManagement;
using static Hermes.Global;

namespace Hermes
{
    
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class LoginMainWindow : Window
    {
       
        public LoginMainWindow()
        {
            LoggingManagement.WriteLog("Starting Hermes.", Level.Info);
            tcpClient = new ClientTcp();
            tcpClient.ConnectToServer();
            InitializeComponent();
        }

        private void HeaderGrid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Frame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void Ellipse_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Close();
        }

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Label_MouseDown(object sender, MouseButtonEventArgs e)
        {
            SignInCanvas.Visibility = Visibility.Hidden;
            SignUpCanvas.Visibility = Visibility.Visible;
            SignInButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF787878"));
            SignUpButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
        }

        private void Label_MouseDown_1(object sender, MouseButtonEventArgs e)
        {
            SignInCanvas.Visibility = Visibility.Visible;
            SignUpCanvas.Visibility = Visibility.Hidden;
            SignUpButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FF787878"));
            SignInButton.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFFFFFF"));
        }

        private void UpdateSignUpProcess(string returnCode, string u)
        {
            switch (returnCode)
            {
                case SERVER_UNREACHABLE:
                    StatusLabel.Text = "The server cannot be reached, make sure that Hermes can access the internet.";
                    break;
                case USER_ALREADY_EXISTS:
                    StatusLabel.Text = "This username is already taken, please modify it and try again.";
                    SignUpUsernameTextbox.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFF0000"));
                    break;
                case USER_CREATED_SUCCESFULLY:
                    StatusLabel.Text = "The account has been created successfully, you can now log in.";
                    break;
                    
            }
            SignUpCommitButton.Content = "Create Account";
            Console.WriteLine("Server answered with " + returnCode);
        }

        private delegate void SignUpDoneDelegate(string returnCode);

        private void SignUpCommitButton_Click(object sender, RoutedEventArgs e)
        {
            SignUpCommitButton.Content = "Creating account ...";

            tcpClient.Register(SignUpUsernameTextbox.Text, SignUpPasswordBox.Password, new LoginDoneDelegate(UpdateSignUpProcess));
        }

        private void HeaderFrame_Navigated(object sender, NavigationEventArgs e)
        {

        }

        private delegate void LoginDoneDelegate(string returnCode, string b64);

        private void LoginCommitButton_Click(object sender, RoutedEventArgs e)
        {
            // Indique a l'utilisateur que le processus de connection est lancé
            LoginCommitButton.Content = "Logging In ...";

            tcpClient.Login(LoginUsernameTexbox.Text, LoginPasswordTextbox.Password, new LoginDoneDelegate(UpdateLoginProcess));

            //UserManagement.LoginUser(LoginUsernameTexbox.Text, LoginPasswordTextbox.Password, new LoginDoneDelegate(UpdateLoginProcess));
        }

        private void UpdateLoginProcess(string returnCode, string b64)
        {
            switch (returnCode)
            {
                case "LOGIN_SUCCESFUL":
                    this.Visibility = Visibility.Hidden;
                 
                    Application.Current.MainWindow = new MainWindow(LoginUsernameTexbox.Text, b64);
                    Application.Current.MainWindow.Show();
                    break;
                case SERVER_UNREACHABLE:
                    StatusLabel.Text = "The server cannot be reached, make sure that Hermes can access the internet.";
                    break;
            }
            SignUpCommitButton.Content = "Login Account";
            Console.WriteLine("Server answered with " + returnCode);
        }
    }
}
