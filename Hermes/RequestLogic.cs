﻿using Leaf.xNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;
using static Hermes.Consts;

namespace Hermes
{
    public static class RequestLogic
    {
        public static void PostAndHandleResponseAsync(string relativeURL, string postData, Delegate Callback)
        {
            new Thread(() =>
            {
                // On déclare la valeur retournée en amont afin d'éviter une erreur d'accés à une variable nulle <--- AH LE CRINGE
                string returnValue = "";

                // On déclare la variable avec le mot clé using afin de libérer les ressources à la fin de l'éxecution du bloc entre parenthèses.
                using (HttpRequest loginUserRequest = new HttpRequest())
                {
                    // L'authorization permet d'"empêcher" de reproduire une requête ailleurs qu'ici (en réalité on peut la retrouver facilement et la falsifier)
                    loginUserRequest.Authorization = "basic " + AUTH_HEADER;

                    // Le user-agent de la requête indique la platforme d'envoi ainsi que certaines info sur cette derniere.
                    loginUserRequest.UserAgent = USER_AGENT;

                    // On indique que l'on souhaite récuperer les données que le serveur à envoyé peu importe l'erreur 
                    loginUserRequest.IgnoreProtocolErrors = true;

                    try
                    {
                        // On construit le corps de la requête selon cette structure (Voir : https://developer.mozilla.org/fr/docs/Web/HTTP/M%C3%A9thode/POST)
                        string answer = loginUserRequest.Post(BACKEND_HOSTNAME + relativeURL, postData, "application/x-www-form-urlencoded").ToString();

                        // On tente de convertir la réponse en entier (int) 
                        try
                        {
                            returnValue = answer; 
                        }
                        catch (Exception)
                        {
                            // Le serveur n'a pas retourné une reponse correcte, il y a surement un problème au niveau du serveur.
                            returnValue = UNKNOWN_ERROR;
                        }
                    }
                    catch (Exception ex)
                    {
                        // Le serveur n'a pas retourné de réponse, il n'est peut être pas joignable pour le moment.
                        returnValue = SERVER_UNREACHABLE;
                    }
                }
                
                // Tout est fini, on peut appeller la fonction demandée avec la valeur retournée
                Application.Current.Dispatcher.Invoke(Callback, returnValue);
            }).Start();
        }
    }
}
