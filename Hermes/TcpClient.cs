﻿using Hermes.RSAImplementation;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

using static Hermes.Global;
using static Hermes.Consts;

namespace Hermes
{
    public class ClientTcp
    {
        /// <summary>
        /// Le client réseau
        /// </summary>
        public TcpClient client = null;

        /// <summary>
        /// Une liste de tout les packets arrivés et non traités, il fonctionne de façon FIFO, First In First Out, le premier paquet arrivé est traité en priorité.
        /// </summary>
        public List<Packet> PacketToTreat = new List<Packet>();

        /// <summary>
        /// Un objet utilisé pour garantir que deux threads n'accédent pas a la liste de packets en même temps.
        /// </summary>
        private object Lock = new object();

        /// <summary>
        /// Authorization token permettant l'envoi de paquets sans envoyer le nom d'utilisateur et le mot de passe.
        /// </summary>
        public string AuthorizationToken = "";

        public Thread ServerCommunicationThread = null;

        public Thread MessageManagementThread = null;

        public Thread UserListThread = null;

        public Dictionary<string, (byte[], byte[])> CryptographicsInfoHolder = new Dictionary<string, (byte[], byte[])>();

        public ClientTcp()
        {
            client = new TcpClient();
        }

        /// <summary>
        /// Permet de récuperer un paquet en le retirant de la liste.
        /// </summary>
        /// <param name="filters">Filtre permettant le tri en se basant sur le commandName du packet.</param>
        /// <returns></returns>
        private Packet PopPacket(string[] filters)
        {
            if(filters.Length == 0)
            {
                // On ne souhaite pas un paquet en particulier.
             
                Packet returnPacket;
                 
                if(PacketToTreat.Count != 0)
                {
                    lock (Lock)
                    {
                        returnPacket = PacketToTreat[0];
                        PacketToTreat.Remove(returnPacket);
                    }
                    return returnPacket;
                }
                else
                {
                    return null;
                }                 
                        
            }
            else
            {
                lock (Lock)
                {
                    foreach (Packet packet in PacketToTreat)
                    {
                        foreach (string filter in filters)
                        {
                            if (packet.CommandName.Contains(filter))
                            {
                                
                                PacketToTreat.Remove(packet);
                                return packet;

                            }
                        }
                    }
                }             

                return null;
            }
        }

        private int CheckSum(byte[] array)
        {
            int checksum = 0;

            foreach (byte item in array)
            {
                checksum += item;
            }

            return checksum;
        }

        public void SendPacket(Packet packet)
        {
            if (!client.Connected)
                throw new Exception("The client isn't connected.");

            packet.AuthorizationToken = AuthorizationToken;

            byte[] array = dfh.Encrypt(ServerPublicKey, packet);


            Console.WriteLine("[Debug] Sending packet " + CheckSum(array) + " " + dfh.PublicKey[10] + dfh.PublicKey[11] + " with IV=" + BitConverter.ToUInt16(dfh.IV, 0));

            
            client.GetStream().Write(BitConverter.GetBytes(array.Length), 0, sizeof(Int32));
            client.GetStream().Write(dfh.IV, 0, dfh.IV.Length);
            client.GetStream().Write(array, 0, array.Length);
        }

        public Packet ReceivePacket()
        {
            if (!client.Connected)
                throw new Exception("The client isn't connected.");
   
            byte[] sizePacket = BitConverter.GetBytes(0);
            byte[] NewIV = new byte[16];

            // Get the size packet
            client.Client.Receive(sizePacket, sizePacket.Length, SocketFlags.None);
            int sp = BitConverter.ToInt32(sizePacket, 0);

            client.Client.Receive(NewIV, NewIV.Length, SocketFlags.None);

            byte[] encryptedBytes = new byte[sp];
            client.Client.Receive(encryptedBytes, encryptedBytes.Length, SocketFlags.None);

            Console.WriteLine("Trying to decrypt packet with IV : " + BitConverter.ToUInt16(NewIV, 0));

            return dfh.Decrypt(ServerPublicKey, encryptedBytes, NewIV);

        }

        public void Register(string username, string password, Delegate Callback)
        {
            Packet registerPacket = new Packet("USER_REGISTER", username + "|" + password);

            SendPacket(registerPacket);

            Packet registerResponsePacket = null;

            while (registerResponsePacket == null) registerResponsePacket = PopPacket(new string[] { "USER_CREATED", "USER_ALREADY_EXISTS" });

            Application.Current.Dispatcher.Invoke(Callback, registerResponsePacket.CommandName[0], "");
        }

        public void Login(string username, string password, Delegate Callback)
        {
            Packet loginPacket = new Packet("USER_LOGIN", username + "|" + password);

            // On tente de se connecter 
            SendPacket(loginPacket);

            // Et puis on lit la réponse du serveur.
            Packet loginResponsePacket = null;

            while (loginResponsePacket == null) {
                loginResponsePacket = PopPacket(new string[] { "INVALID_LOGIN", "LOGIN_SUCCESFUL" });
                Thread.Sleep(10);
            }

            AuthorizationToken = !String.IsNullOrEmpty(loginResponsePacket.AuthorizationToken) ? loginResponsePacket.AuthorizationToken : "";

            Application.Current.MainWindow.Dispatcher.Invoke(Callback, loginResponsePacket.CommandName, loginResponsePacket.CommandArguments[0]);

            UserListThread = new Thread(UserListLoop);
            UserListThread.Name = "Thread de rafraichissement de liste user.";
            UserListThread.Start();
        }

        /// <summary>
        /// Cette boucle va servir de "centre d'arrivage", elle va s'occuper de récuperer et transmettre les packets entrants.
        /// </summary>
        public void ServerCommunicationLoop()
        {
            while (client.Connected)
            {
                RETRY:
                Packet receivedPacket = null;
                try
                {
                    receivedPacket = ReceivePacket();
                }
                catch (Exception)
                {
                    goto RETRY;
                    
                }

                lock (Lock)
                {
                    PacketToTreat.Add(receivedPacket);                    
                }            
            }
        }

        public void DebugMessageReceived(Packet packet)
        {
            MessageObject mo = (MessageObject)packet.CommandArguments[0];
            while (MainWindow.main == null)
            {

                Thread.Sleep(10);
            }

            string base64image = "";

            try
            {
                base64image = TotalUsers.Find(x => x.Username == packet.CommandSender).AvatarBase64;
            }
            catch (Exception)
            {
                
            }

            mo.Sender = String.IsNullOrEmpty(packet.CommandSender) ? mo.Sender : packet.CommandSender;

            MainWindow.main.Dispatcher.Invoke(new Action(() => ((MainWindow)Application.Current.MainWindow).AddMessageToConversation(mo, base64image)));

            Console.WriteLine("You received a message ! ");
            Console.WriteLine("From : " + packet.CommandSender);
            Console.WriteLine("At : " + mo.SentAt.ToShortTimeString());
            Console.WriteLine("Content : " + mo.Content);
        }

        public void UserListLoop()
        {
            while (client.Connected)
            {
                SendPacket(new Packet("USER_LIST", ""));

                Thread.Sleep(USER_LIST_REFRESH_DELAY);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="receiverUsername"></param>
        /// <returns>Booléen, true si l'opération a réussi.</returns>
        public bool ConnectionLink(string receiverUsername)
        {

            SendPacket(new Packet("CONNECTION_LINK_REQUEST", new ConnectionLinkObject("", receiverUsername, dfh.PublicKey, dfh.IV)));

            Packet clKey = null;
            while (clKey == null)
            {
                clKey = PopPacket(new string[] { "CONNECTION_LINK" });
                Thread.Sleep(10);
            }

            if (clKey.CommandName == "CONNECTION_LINK_ERROR")
                return false;
  
            ConnectionLinkObject clO = (ConnectionLinkObject)clKey.CommandArguments[0];
            
            CryptographicsInfoHolder.Add(clO.Receiver, (clO.PublicKey, clO.IV));


            return true;
        }

        public bool ConnectionLinkCheck(string receiverUsername)
        {
            SendPacket(new Packet("CONNECTION_LINK_CHECK", receiverUsername));

            Packet answerPacket = null;
            while (answerPacket == null)
            {
                answerPacket = PopPacket(new string[] { "CONNECTION_LINK_CHECK_ANSWER" });
                Thread.Sleep(1);
            }

            return (bool)answerPacket.CommandArguments[0];
        }
        
        public void SendPacketTransfer(string receiver, Packet content)
        {
            byte[] IV = dfh.IV;

            byte[] encryptedData = dfh.Encrypt(CryptographicsInfoHolder[receiver].Item1, content);

            Packet packetToSend = new Packet("PACKET_TRANSFER", new object[] { receiver, encryptedData, dfh.IV });

            SendPacket(packetToSend);
        }

        public void PacketTransfer(Packet receivedPacket)
        {

            (byte[], byte[]) EncryptionObject = (null, null);

            try
            {
                EncryptionObject = CryptographicsInfoHolder[(string)receivedPacket.CommandArguments[0]];
            }
            catch (Exception)
            {
                try
                {
                    Console.WriteLine("Couldn't decrypt a packet transfered from " + (string)receivedPacket.CommandArguments[0] + " because we didn't hold any crypto info from him.");
                }
                catch (Exception)
                {
                    
                }
                
                return;
            }
            

            Packet decryptedPacket = dfh.DecryptPacket(EncryptionObject.Item1, (byte[])receivedPacket.CommandArguments[1], receivedPacket.PacketIV);

            decryptedPacket.CommandSender = (string)receivedPacket.CommandArguments[0];


            lock (Lock)
            {
                PacketToTreat.Add(decryptedPacket);
            }

            Console.WriteLine("Received transfered packet from " + (string)receivedPacket.CommandArguments[0]);
        }

        public void ConnectionLinkRequest(ConnectionLinkObject clo)
        {
            CryptographicsInfoHolder.Add(clo.Sender, (clo.PublicKey, clo.IV));
            SendPacket(new Packet("CONNECTION_LINK_ANSWER", new ConnectionLinkObject(clo.Sender, clo.Receiver, dfh.PublicKey, dfh.IV)));
        }

      

        public void PacketManagementLoop()
        {
            while (client.Connected)
            {
                bool shouldContinue = false;
                Packet packetToTreat = null;

                lock (Lock)
                {
                    shouldContinue = PacketToTreat.Count != 0;

                    if (shouldContinue)
                    {
                        packetToTreat = PacketToTreat[0];
                        PacketToTreat.Remove(packetToTreat);
                    }
                }

                if(shouldContinue)
                {
                    try
                    {

                        switch (packetToTreat.CommandName)
                        {
                            case "CONNECTION_LINK_REQUEST":
                                ConnectionLinkRequest((ConnectionLinkObject)packetToTreat.CommandArguments[0]);
                                break;
                            case "MESSAGE_RECEIVED":
                                DebugMessageReceived(packetToTreat);
                                break;
                            case "USER_LIST_ANSWER":
                                UserListObject ulo = (UserListObject)packetToTreat.CommandArguments[0];

                                ConnectedUsers = new List<UserObject>(ulo.ConnectedUsers);
                                TotalUsers = new List<UserObject>(ulo.AllUsers);

                                MainWindow.main.Dispatcher.Invoke(new Action(() => ((MainWindow)Application.Current.MainWindow).UpdateUsersList()));
                                break;
                            case "PACKET_TRANSFER":
                                PacketTransfer(packetToTreat);
                                break;
                            default:
                                lock (Lock)
                                {
                                    PacketToTreat.Add(packetToTreat);
                                }
                                
                                break;
                        }
                    }
                    catch (Exception)
                    {
                            
                    }   
                }

                Thread.Sleep(5);
            }
        }

        public void ConnectToServer()
        {
            try
            {
                client.Connect("127.0.0.1", 8080);

                // On lit les 140 premiers octets du flux représentant la clé publique du serveur.
                client.GetStream().Read(ServerPublicKey, 0, 140);

                // Et on envoie la notre 
                client.GetStream().Write(dfh.PublicKey, 0, dfh.PublicKey.Length);
                
                // On récupère l'IV
                client.GetStream().Read(ServerIV, 0, ServerIV.Length);
              
                // Et on envoie le notre.
                client.GetStream().Write(dfh.IV, 0, dfh.IV.Length);

                // On communique maintenant en packet, on démarre donc le centre d'arrivage.
                ServerCommunicationThread = new Thread(ServerCommunicationLoop);
                ServerCommunicationThread.Name = "Thread d'écoute serverur.";
                ServerCommunicationThread.Start();

                MessageManagementThread = new Thread(PacketManagementLoop);
                MessageManagementThread.Name = "Thread de management de packet.";
                MessageManagementThread.Start();

                



            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't connect to hermes server.", "Hermes - Error");
            }
        }
    }
}
