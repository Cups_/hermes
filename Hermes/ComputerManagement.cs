﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    public static class ComputerManagement
    {
        public static string GetComputerName()
        {
            return Environment.MachineName;
        }
    }
}
