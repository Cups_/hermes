﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hermes.Controls
{


    /// <summary>
    /// Interaction logic for ConversationControl.xaml
    /// </summary>
    public partial class ConversationControl : UserControl
    {
        public Color InactiveColor = Color.FromRgb(153, 153, 153);
        public Color ConnectedColor = Color.FromRgb(65, 153, 77);
        public string ConversationID = "";

        public delegate void ClickEventHandler(ConversationControl sender);
        public event ClickEventHandler ClickEvent;
        public bool Enabled = false;
        
        /// <summary>
        /// Le nom d'utilisateur de l'envoyeur.
        /// </summary>
        public string Username { get; set; }

        public bool Connected { get; set; }

        public List<MessageObject> MessageObjects = new List<MessageObject>();

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        public ConversationControl(string username, string base64image, bool connected, string conversationID = "")
        {
            InitializeComponent();
            UsernameLabel.Content = username;
            Username = username;
            Connected = connected;
            ConversationID = conversationID;

            try
            {
                if(base64image != "")
                {

                    ImageBrush avatarBrush = new ImageBrush();

                    avatarBrush.ImageSource = LoadImage(Convert.FromBase64String(base64image));

                    UserAvatar.Fill = avatarBrush;

                }
            }
            catch (Exception)
            {
                
            }

            Update();
        }

        public void AddMessage(MessageObject mo)
        {
            MessageObjects.Add(mo);
        }

        private void Update()
        {
            if(this.Connected)
                ConnectedElipse.Fill = new SolidColorBrush(ConnectedColor);
            else
                ConnectedElipse.Fill = new SolidColorBrush(InactiveColor);

            if (Enabled)
            {
                ControlMessage.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#3E4F63"));
                
            }
            else
            {
                ControlMessage.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#363E47"));
            }
        }

        public void SetEnabled(bool enabled)
        {
            Enabled = enabled;
            Update();
        }

        public void ToggleEnabled()
        {
            Enabled = !Enabled;
            Update();
        }

        public void SetLatestMessage(string message)
        {
            LatestMessageLabel.Content = message;
        }

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ClickEvent != null)
                ClickEvent(this);
        }

        private void ControlMessage_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (ClickEvent != null)
                ClickEvent(this);
        }
    }
}
