﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static Hermes.Global;

namespace Hermes.Controls
{
    /// <summary>
    /// Interaction logic for ChatControl.xaml
    /// </summary>
    public partial class ChatControl : UserControl
    {
        ConversationControl holder = null;

        public ChatControl()
        {
            InitializeComponent();
            
        }

        public void SetHolder(ref ConversationControl conv)
        {
            holder = conv;
        }

        public void DrawMessages()
        {
            MessagePanel.Children.Clear();

            
            foreach (MessageObject messageObject in holder.MessageObjects)
            {
                string b64image = "";

                if (messageObject.Sender == "You")
                {
                    b64image = Base64Avatar;
                }
                else
                {
                    b64image = TotalUsers.Find(member => member.Username == messageObject.Sender).AvatarBase64;
                }
           

                DrawMessage(messageObject, b64image);
            }
        }

        public void AddMessage(MessageObject message)
        {
            holder.MessageObjects.Add(message);

            string b64image = "";

            if (message.Sender == "You")
            {
                b64image = Base64Avatar;
            }
            else
            {
                b64image = TotalUsers.Find(member => member.Username == message.Sender).AvatarBase64;
            }


            DrawMessage(message, b64image);
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void DrawMessage(MessageObject message, string b64Image)
        {
            MessageControl mc = new MessageControl();
            ImageBrush avatarBrush = new ImageBrush();

            avatarBrush.ImageSource = LoadImage(Convert.FromBase64String(b64Image));
            mc.SetMessage(message.Content);
            mc.SenderLabel.Content = message.Sender;
            mc.MessageUserImage.Fill = avatarBrush;
            MessagePanel.Children.Add(mc);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {   
            AddMessage(new MessageObject("You", MessageTextbox.Text, DateTime.Now));           

            tcpClient.SendPacketTransfer(holder.Username, new Packet("MESSAGE_RECEIVED", new MessageObject(Username, MessageTextbox.Text, DateTime.Now)));

            MessageTextbox.Text = "";

        }
    }
}
