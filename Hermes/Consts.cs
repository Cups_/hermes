﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Hermes
{
    public static class Consts
    {
        public const string BACKEND_HOSTNAME = "127.0.0.1";
        public const string AUTH_HEADER = "SGVybWVzOkN1cHM=";
        public const string USER_AGENT = "Hermes Desktop Client";

        public const int USER_LIST_REFRESH_DELAY = 10000;

        #region RETURN VALUES
        // [GENERAL RETURNED VALUES -1 - 10]
        public const string UNKNOWN_ERROR = "ERROR";
        public const string DEFAULT_SUCCESS = "SUCCESS";
        public const string INVALID_AUTHORIZATION = "INVALID_AUTH";
        public const string SERVER_UNREACHABLE = "SERVER_UNREACHABLE";

        // [USER CREATION RETURNED VALUES 11 - 20]
        public const string USER_CREATED_SUCCESFULLY = "USER_CREATED";
        public const string USER_ALREADY_EXISTS = "USER_EXISTS";

        // [USER LOGGING RETURNED VALUES 21 - 30]
        public const string USER_NEEDS_CONFIRMATION = "USER_NEEDS_CONFIRMATION";
        public const string USER_INVALID_PASSWORD = "USER_INVALID_PASSWORD";
       
        #endregion
    }
}
