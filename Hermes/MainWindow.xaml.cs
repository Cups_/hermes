﻿using Hermes.Controls;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using static Hermes.Global;

namespace Hermes
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static MainWindow main;

        private List<ConversationControl> Conversations = new List<ConversationControl>();

        public MainWindow(string username, string base64Avatar)
        {
            InitializeComponent();
            main = this;
            Username = username;
            Base64Avatar = base64Avatar;
            UsernameLabel.Content = "Welcome, " + username;
            SetUserAvatar(base64Avatar);
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;
            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }
            image.Freeze();
            return image;
        }

        private void SetUserAvatar(string b64)
        {
            ImageBrush avatarBrush = new ImageBrush();

            avatarBrush.ImageSource = LoadImage(Convert.FromBase64String(b64));
            LocalAvatarEllipse.Fill = avatarBrush;
        }

        private void Exit()
        {
            Environment.Exit(0);
        }

        private void AddButton_Click(object sender, RoutedEventArgs e)
        {
            ConversationControl convControl = new ConversationControl("Random User #" + new Random().Next(), "", true);

            // On enregistre l'event afin de pouvoir executer du code quand le controle est cliqué 
            convControl.ClickEvent += ConvControl_ClickEvent;

            // On ajoute le controle au panneau
            Conversations.Add(convControl);
            ConversationPanel.Children.Add(convControl);
        }
        
        public void UpdateUsersList()
        {
            // TODO, is this really useful? 
        }

        public void AddMessageToConversation(MessageObject message, string base64image)
        {
            ConversationControl convControl = null;

            try
            {
                convControl = Conversations.Find(conversation => conversation.Username == message.Sender);

                convControl.SetLatestMessage(message.Content);
                
                convControl.MessageObjects.Add(message);

                Chat.DrawMessages();
            }
            catch (Exception)
            {
                
            }

            if(convControl == null)
            {
                convControl = new ConversationControl(message.Sender, base64image, true);

                convControl.ClickEvent += ConvControl_ClickEvent;
                convControl.MessageObjects.Add(message);
                convControl.SetLatestMessage(message.Content);
                Conversations.Add(convControl);
                ConversationPanel.Children.Add(convControl);
            }
            
        }

        private void ConvControl_ClickEvent(ConversationControl a)
        {
            a.ToggleEnabled();

            Chat.MessagePanel.Children.Clear();

            if (a.Enabled)
            {
                // On vérifie si l'utilisateur en face est lié.
                bool isLinked = tcpClient.ConnectionLinkCheck(a.Username);

                if (!isLinked)
                {
                    bool linkedSuccess = tcpClient.ConnectionLink(a.Username);

                    if (!linkedSuccess)
                    {
                        a.MessageObjects.Add(new MessageObject("Local", "Une connexion sécurisée avec votre correspondant n'a pas pu être établie.", DateTime.Now));

                    }
                    else
                    {
                        // TODO : REMOVE DEBUG 
                        tcpClient.SendPacketTransfer(a.Username, new Packet("MESSAGE_RECEIVED", new MessageObject("", "Hi, looks like we just linked !", DateTime.Now))); // There is no need to specify the sender, the server will automatically find it using the token.
          
                    }
                }


                Chat.SetHolder(ref a);

                Chat.UsernameLabel.Content = "@" + a.Username;    
            }

            // On désactive les autres controles
            foreach (var children in ConversationPanel.Children)
            {
                if((ConversationControl)children != a)
                {
                    ((ConversationControl)children).SetEnabled(false);
                }
            }
        }

        private void CloseButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (tcpClient.client.Connected)
            {
                tcpClient.SendPacket(new Packet("USER_DISCONNECT", ""));
                tcpClient.client.Close();
            }
                
            // On ferme l'application 
            Exit();
        }

        private void MovingFrame_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        private void MinimizeButton_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }



        List<ConversationControl> ConversationHolder = new List<ConversationControl>();
        bool UpdateHolderFlag = false;
        private void SearchMembersTextbox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (!UpdateHolderFlag)
            {
                foreach (var controlchild in ConversationPanel.Children)
                {
                    ConversationHolder.Add((ConversationControl)controlchild);
                }

                ConversationPanel.Children.Clear();

                foreach (UserObject dbMember in TotalUsers)
                {
                    bool isConnected = ConnectedUsers.Find(member => member.Username == dbMember.Username) != null;

                    ConversationControl convControl = new ConversationControl(dbMember.Username, dbMember.AvatarBase64, isConnected);
                    convControl.ClickEvent += ConvControl_ClickEvent1;
                    ConversationPanel.Children.Add(convControl);
                }

                UpdateHolderFlag = true;
            }
        }

        private void ConvControl_ClickEvent1(ConversationControl sender)
        {
            // On ajoute le nouvel utilisateur choisi à l'historique de conversation
            sender.ClickEvent += ConvControl_ClickEvent;
            Conversations.Add(sender);
            ConversationHolder.Add(sender);

            // On ferme la recherche.
            if (UpdateHolderFlag)
            {
                ConversationPanel.Children.Clear();

                foreach (var item in ConversationHolder)
                {
                    try
                    {
                        ConversationPanel.Children.Add(item);
                    }
                    catch (Exception)
                    {
                        
                    }
                    
                }

                ConversationHolder.Clear();

                UpdateHolderFlag = false;
            }
        }

        private void SearchMembersTextbox_LostFocus(object sender, RoutedEventArgs e)
        {
            
        }

        private void CloseConvButton_Click(object sender, RoutedEventArgs e)
        {
            if (UpdateHolderFlag)
            {
                ConversationPanel.Children.Clear();

                foreach (var item in ConversationHolder)
                {
                    ConversationPanel.Children.Add(item);
                }

                ConversationHolder.Clear();

                UpdateHolderFlag = false;
            }
        }

        private string GetBase64ImageFromPath(string path)
        {
            byte[] imageArray = System.IO.File.ReadAllBytes(path);
            return Convert.ToBase64String(imageArray);
        }
        
        private void LocalAvatarEllipse_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.FileName = "";
            openFileDialog.CheckFileExists = false;
            openFileDialog.Title = "Hermes - Choose a new avatar";
            openFileDialog.Filter = "Image File|*.png;*.jpg";

            if (openFileDialog.ShowDialog().Value)
            {
                string b64 = GetBase64ImageFromPath(openFileDialog.FileName);

                tcpClient.SendPacket(new Packet("USER_UPDATE_AVATAR", b64));

                SetUserAvatar(b64);
            }
        }
    }
}
