﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Hermes.RSAImplementation
{
    public class DiffieHellman
    {
        private Aes aes = null;
        private ECDiffieHellmanCng diffieHellman = null;

        private readonly byte[] publicKey;

        public byte[] PublicKey
        {
            get
            {
                return this.publicKey;
            }
        }

        /// <summary>
        /// Décrypte un tableau d'octets entrant et le converti en Packet
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="encryptedMessage"></param>
        /// <param name="iv"></param>
        /// <returns>Le Packet converti.</returns>
        public Packet Decrypt(byte[] publicKey, byte[] encryptedMessage, byte[] iv)
        {
            // On crée un flux de mémoire qui va contenir les octets décryptés du message
            using (MemoryStream sr = new MemoryStream(DecryptBytes(publicKey, encryptedMessage, iv)))
            {
                // On crée le déserialisateur.
                var binFormat = new BinaryFormatter();
                binFormat.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

                // On indique au déserializateur qu'il doit autoriser la déserialisation peut importe d'ou vient le packet.
                binFormat.Binder = new AllowAllAssemblyVersionsDeserializationBinder();

                // On désérialize le flux de mémoire afin d'obtenir le packet.
                return (Packet)binFormat.Deserialize(sr);
            }     
        }

        /// <summary>
        /// Simple fonction pour décrypter des tableaux d'octets.
        /// </summary>
        /// <param name="publicKey"></param>
        /// <param name="encryptedMessage"></param>
        /// <param name="iv"></param>
        /// <returns></returns>
        public byte[] DecryptBytes(byte[] publicKey, byte[] encryptedMessage, byte[] iv)
        {
            // On importe la clé.
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);

            // Et on en crée une dérivée (mixture de privée et publique)
            var derivedKey = this.diffieHellman.DeriveKeyMaterial(key);

            // On crée le décrypteur
            this.aes.Key = derivedKey;
            this.aes.IV = iv;
            this.aes.Padding = PaddingMode.Zeros;

            using (var plainText = new MemoryStream())
            {
                using (var decryptor = this.aes.CreateDecryptor())
                {
                    using (var cryptoStream = new CryptoStream(plainText, decryptor, CryptoStreamMode.Write))
                    {
                        // On écrit les octets dans le flux décrypteur
                        cryptoStream.Write(encryptedMessage, 0, encryptedMessage.Length);
                    }
                }

                // On retourne le tableau décrypté final.
                return plainText.ToArray();
            }
        }


        /// <summary>
        /// Fonction permettant de retirer les 0 finaux d'un tableau.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static byte[] TrimEnd(byte[] array)
        {
            // Simple expression Linq pour trouver le début de la suite de 0 finale.
            int lastIndex = Array.FindLastIndex(array, b => b != 0);

            // On retire cette suite finale 
            Array.Resize(ref array, lastIndex + 1);

            // Et on retourne le nouveau tableau.
            return array;
        }


        public Packet DecryptPacket(byte[] publicKey, byte[] encryptedMessage, byte[] iv)
        {
            byte[] DecryptedData = DecryptBytes(publicKey, encryptedMessage, iv);

            using ( MemoryStream sr = new MemoryStream(DecryptedData))
            {
                var binFormat = new BinaryFormatter();
                binFormat.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;
                binFormat.Binder = new AllowAllAssemblyVersionsDeserializationBinder();

                return (Packet)binFormat.Deserialize(sr);
            }
        }

        public byte[] IV
        {
            get
            {
                return this.aes.IV;
            }
        }

        public byte[] Encrypt(byte[] publicKey, byte[] packet)
        {
            byte[] encryptedMessage;
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.diffieHellman.DeriveKeyMaterial(key); // "Common secret"

            this.aes.Key = derivedKey;
            this.aes.Padding = PaddingMode.Zeros;

            BinaryFormatter bf = new BinaryFormatter();

            using (var cipherText = new MemoryStream())
            {
                using (var encryptor = this.aes.CreateEncryptor())
                {
                    using (var cryptoStream = new CryptoStream(cipherText, encryptor, CryptoStreamMode.Write))
                    {
                        cryptoStream.Write(packet, 0, packet.Length);
                    }
                }

                encryptedMessage = cipherText.ToArray();
            }

            return encryptedMessage;
        }

        public byte[] Encrypt(byte[] publicKey, Packet packet)
        {
            byte[] encryptedMessage;
            var key = CngKey.Import(publicKey, CngKeyBlobFormat.EccPublicBlob);
            var derivedKey = this.diffieHellman.DeriveKeyMaterial(key); // "Common secret"

            this.aes.Key = derivedKey;
            this.aes.Padding = PaddingMode.Zeros;

            BinaryFormatter bf = new BinaryFormatter();
            bf.AssemblyFormat = System.Runtime.Serialization.Formatters.FormatterAssemblyStyle.Simple;

            using (var cipherText = new MemoryStream())
            {
                using (var encryptor = this.aes.CreateEncryptor())
                {
                    using (var cryptoStream = new CryptoStream(cipherText, encryptor, CryptoStreamMode.Write))
                    {
                        bf.Serialize(cryptoStream, packet);
                    }
                }

                encryptedMessage = cipherText.ToArray();
            }

            return encryptedMessage;
        }

        public DiffieHellman()
        {
            this.aes = new AesCryptoServiceProvider();

            this.diffieHellman = new ECDiffieHellmanCng
            {
                KeyDerivationFunction = ECDiffieHellmanKeyDerivationFunction.Hash,
                HashAlgorithm = CngAlgorithm.Sha256
            };

            // This is the public key we will send to the other party
            this.publicKey = this.diffieHellman.PublicKey.ToByteArray();
        }
    }

    sealed class AllowAllAssemblyVersionsDeserializationBinder : System.Runtime.Serialization.SerializationBinder
    {
        public override Type BindToType(string assemblyName, string typeName)
        {
            Type typeToDeserialize = null;

            String currentAssembly = Assembly.GetExecutingAssembly().FullName;

            // On force la déserialization à correspondre à l'assembly actuel.
            assemblyName = currentAssembly;

            typeToDeserialize = Type.GetType(String.Format("{0}, {1}",
                typeName, assemblyName));

            return typeToDeserialize;
        }
    }
}
