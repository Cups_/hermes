﻿using Hermes.RSAImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    public static class Global
    {
        public static ClientTcp tcpClient = new ClientTcp();
        public static DiffieHellman dfh = new DiffieHellman();
        public static byte[] ServerPublicKey = new byte[140];
        public static byte[] ServerIV = new byte[16];

        public static List<UserObject> ConnectedUsers = null;

        public static List<UserObject> TotalUsers = null;

        public static string Username = null;
        public static string Base64Avatar = null;

        public static int CheckSum(byte[] array)
        {
            int checksum = 0;

            foreach (byte item in array)
            {
                checksum += item;
            }

            return checksum;
        }

    }
}
