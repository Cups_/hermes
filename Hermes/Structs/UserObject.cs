﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    [Serializable]
    public class UserObject
    {

        /// <summary>
        /// Le nom d'utilisateur du membre
        /// </summary>
        public string Username { get; set; }

        /// <summary>
        /// L'avatar du client, en base64.
        /// </summary>
        public string AvatarBase64 { get; set; }


        public UserObject(string Username, string AvatarBase64)
        {
            this.Username = Username;
            this.AvatarBase64 = AvatarBase64;
        }
    }
}
