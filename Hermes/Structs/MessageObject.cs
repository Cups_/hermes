﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    [Serializable]
    public class MessageObject
    {
        /// <summary>
        /// L'auteur du message.
        /// </summary>
        public string Sender;

        /// <summary>
        /// Le contenu du message.
        /// </summary>
        public string Content;

        /// <summary>
        /// La date d'envoi du message. 
        /// </summary>
        public DateTime SentAt;


        public MessageObject(string Sender, string Content, DateTime SentAt)
        {
            this.Sender = Sender;
            this.Content = Content;
            this.SentAt = SentAt;
        }
    }
}
