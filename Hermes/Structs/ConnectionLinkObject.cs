﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    [Serializable]
    public class ConnectionLinkObject
    {
        public string Sender { get; set; }
        public string Receiver { get; set; }

        /// <summary>
        /// Contient un tableau d'octet servant a contenir les clés ou IV, afin de mettre en relation les deux clients.
        /// </summary>
        public byte[] PublicKey { get; set; }

        public byte[] IV { get; set; }

        public ConnectionLinkObject(string Sender, string Receiver, byte[] PublicKey, byte[] IV)
        {
            this.Sender = Sender;
            this.Receiver = Receiver;
            this.PublicKey = PublicKey;
            this.IV = IV;
        }
    }
}
