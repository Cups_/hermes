﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hermes
{
    public static class LoggingManagement
    {
        public enum Level
        {
            Info,
            Verbose, 
            Error,
            Warning,
            Critical
        }

        public static void WriteLog(string message, Level level)
        {
            TimeSpan timeElapsed = DateTime.UtcNow - System.Diagnostics.Process.GetCurrentProcess().StartTime.ToUniversalTime();
            string levelOutput = "Info";

            switch (level)
            {  
                case Level.Info:
                    levelOutput = "Info";
                    break;
                case Level.Verbose:
                    levelOutput = "Verbose";
                    break;
                case Level.Error:
                    levelOutput = "Error";
                    break;
                case Level.Warning:
                    levelOutput = "Warning";
                    break;
                case Level.Critical:
                    levelOutput = "Critical";
                    break;
                default:
                    break;
            }

            Console.WriteLine(String.Format("[{0}:{1}] | [{2}] : {3}", Convert.ToInt32(timeElapsed.TotalHours).ToString().PadLeft(2, '0'), Convert.ToInt32(timeElapsed.TotalMinutes).ToString().PadLeft(2, '0'), levelOutput, message));
        }
    }
}
